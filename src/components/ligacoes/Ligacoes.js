import React, { Component } from 'react';

import base from './../db/base'; 

class Calcular extends Component {
    constructor(props){
        super(props)
        this.state = {
            simulacoes: {}
        }
    }

    componentDidMount(){
        base.syncState('simulacoes', {
            context: this,
            state: 'simulacoes',
            asArray: false
        });
    }
    render(){
        return(
            <div>
                 <section id='services'>
                    <h3>Simule a sua ligação</h3>
                    <br />
                    <form className='form-inline'>
                        <div className='form-group mb-2'>
                            <label>
                                DD de origin: 
                                <input type='text' name='origem' />
                            </label>
                        </div>
                        <div className='form-group mb-2'>
                            <label>
                            DD de Destino
                            <input type='text' name='destino' />
                            </label>
                        </div>
                        <div className='form-group mb-2'>
                            <label>
                            Tempo da Ligação
                            <input type='text' name='destino' />
                            </label>
                        </div>
                        <div className='form-group mb-2'>
                            <label>
                            E-mail
                            <input type='text' name='destino' />
                            </label>
                        </div>
                        <button type="submit" className="btn btn-primary mb-2">Simular</button>
                    </form>
                </section>

                <section className='page-section'>
                    <h3>Resultado da simulação</h3>
                    <div className='container'>
                        <div className='form-group mb-2'>
                        <p>
                            <span  >DD de origem: 011</span><br />
                            <span>DD de destino: 016</span><br />
                            <span>Plano FaleMais: 60 minutos</span><br />
                            <span>Valor com Plano: R$ 37,40 </span><br />
                            <span>Valor sem Plano: R$ 136,00</span>
                        </p>
                        </div>
                    </div>
                </section>
            </div>
        )
    };
}

export default Calcular;