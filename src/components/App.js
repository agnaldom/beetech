import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

import Header from './Header';
import Home from './home/Home';
import Footer from './Footer';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Header />
          <Route exact  path='/' component={Home} />
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
