import Rebase from 're-base';
import firebase from 'firebase';

const config = {
    apiKey: 'AIzaSyATnEkbVUuYtAL_f-Bc86D-c9arfwh5MWY',
    authDomain: 'beephonica.firebaseapp.com',
    databaseURL: 'https://beephonica.firebaseio.com',
    projectId: 'beephonica',
    storageBucket: '',
    messagingSenderId: '914142565650'
  };

const app = firebase.initializeApp(config);
const base = Rebase.createClass(app.database())

//export const auth = firebase.auth()

export default base;