import React from  'react';

const Tabela = props => {
    return (
        <div>
            <h2>Plano Atual</h2>
                <table  className="table table-dark">
                    <thead>
                        <tr>
                        <td className="text-light">#</td>
                        <td className="text-light">Origem</td>
                        <td className="text-light">Destino</td>
                        <td className="text-light">$/min</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>011</td>
                            <td>016</td>
                            <td>1,90</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>016</td>
                            <td>011</td>
                            <td>2,90</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>011</td>
                            <td>017</td>
                            <td>1,70</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>017</td>
                            <td>011</td>
                            <td>2,70</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>011</td>
                            <td>018</td>
                            <td>0,90</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>018</td>
                            <td>011</td>
                            <td>1,90</td>
                        </tr>
                </tbody>
                </table>
        </div>
    )
}

export default Tabela;